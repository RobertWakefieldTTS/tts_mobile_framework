﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkedRoomLoader : MonoBehaviour {

    public string playerName = "Default_Name";
    public int playerScore = 0;

    public ChoiceChecker.GameChoices mChoice = ChoiceChecker.GameChoices.NONE;

    public Text choice1Text, choice2Text;

    public bool hasReceivedEnemyScore = false;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (PhotonNetwork.connected && PhotonNetwork.inRoom)
        {
            if (GetComponent<PhotonView>().owner == null || !GetComponent<PhotonView>().owner.IsLocal)
            {
                return;
            }
        }
	}


    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(playerName);
            stream.SendNext(playerScore);
            stream.SendNext(mChoice);
            stream.SendNext(hasReceivedEnemyScore);
        }
        else
        {
            playerName = (string)stream.ReceiveNext();
            playerScore = (int)stream.ReceiveNext();
            mChoice = (ChoiceChecker.GameChoices)stream.ReceiveNext();
            hasReceivedEnemyScore = (bool)stream.ReceiveNext();
        }

        Debug.Log("actually photonning");
    }
}
