﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GMS : MonoBehaviour {

    /*************************************************/
    //Singleton Implementation
    private static GMS instance;
    public static GMS Instance
    {
        get { return instance; }
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
    /*************************************************/


    /*************************************************/
    //Universal Variables and stuff (should be edited for each game!)
    public int currency, premiumCurrency;
    

    public enum GameMode
    {
        DEFAULT,

        NUM_GAMEMODES,
    }

    public GameMode mGameMode = GameMode.DEFAULT;
    /*************************************************/


    /*************************************************/
    //Later, Account things will go here

    //Later, Ad Information will go here

    //Later, App ID information will go here

    /*************************************************/


    /*************************************************/
    //Game Specific Variables can go HERE

    /*************************************************/

    // Use this for initialization
    void Start ()
    {
	    
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    //Static Scene Management
    public static void SwitchScene(string cName)
    {
        SceneManager.LoadScene(cName);
    }

    public static void SwitchScene(int cId)
    {
        SceneManager.LoadScene(cId);
    }

    public void SaveGame()
    {
        Debug.Log("Saving Game Data...");
        Debug.Log("Game Data could not be Saved.");
    }

    public void LoadGame()
    {
        Debug.Log("Loading Game Data...");
        Debug.Log("Game Data could not be Loaded.");
    }


}
