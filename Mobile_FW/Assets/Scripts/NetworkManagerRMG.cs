﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkManagerRMG : MonoBehaviour {

    PhotonView mPV;
    public Text pvTEXT;
    public Text onlineStateText;

    bool lookingForRoom = false;

    // Use this for initialization
    void Start () {
        PhotonNetwork.ConnectUsingSettings("v1.0");
	}
	
	// Update is called once per frame
	void Update ()
    {

        if (!PhotonNetwork.connected)
        {
            onlineStateText.text = "Connecting to Server";
        }
        else if (!PhotonNetwork.inRoom && !lookingForRoom)
        {
            onlineStateText.text = "Online";
            GameObject.Find("TotalText").GetComponent<Text>().text = "Total Players: " + PhotonNetwork.countOfPlayersOnMaster;
        }
        else if (PhotonNetwork.inRoom)
        {
            onlineStateText.text = "Connected to Lobby";
            lookingForRoom = false;
        }
        else if (PhotonNetwork.connected && lookingForRoom)
        {
            onlineStateText.text = "Looking for Online Lobby...";
        }
        Debug.Log("iuhehfue");
        //if you cancel looking for a room, please set lookingForRoom to false;
        if (PhotonNetwork.connected && PhotonNetwork.inRoom)
            pvTEXT.text = "Lobby: " + PhotonNetwork.room.PlayerCount.ToString();
        else
            pvTEXT.text = "Lobby: 0";

        if (PhotonNetwork.connected && PhotonNetwork.inRoom && PhotonNetwork.room.PlayerCount == 2)
        {
            GMS.SwitchScene(1);
        }

    }

    void OnPhotonSerializeView()
    {
        Debug.Log("yup we suer are Photoning alright");
    }

    public void JoinRandomRoom()
    {
        if (PhotonNetwork.connected && !PhotonNetwork.inRoom)
        {
            RoomOptions roomOpts = new RoomOptions() { IsVisible = false, MaxPlayers = 4 };
            
            PhotonNetwork.JoinOrCreateRoom("test", roomOpts, TypedLobby.Default);
            lookingForRoom = true;
        }
        else
            Debug.Log("Not connected yet pls wait man");
    }

    //not sure how to cancel the Photon Look request
    public void CancelLookingForRoom()
    {
        lookingForRoom = false;
    }

    public void LeaveRoom()
    {
        if (PhotonNetwork.inRoom)
        {
            PhotonNetwork.LeaveRoom();
            lookingForRoom = false;
        }
    }
}
