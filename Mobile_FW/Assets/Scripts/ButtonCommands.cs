﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonCommands : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void TwitterButton()
    {
        Application.OpenURL("http://www.twitter.com/TooTiredStudios");
    }

    public void WebsiteButton()
    {
        Application.OpenURL("http://www.TooTiredStudios.com");
    }

    public void SwitchSceneToID(int cID)
    {
        GMS.SwitchScene(cID);
    }

    public void SwitchSceneToName(string cName)
    {
        GMS.SwitchScene(cName);
    }

    public void ConnectToRoom()
    {
        NetworkManagerRMG tempNetwork = FindObjectOfType<NetworkManagerRMG>();
        tempNetwork.JoinRandomRoom();
    }
}
