﻿// Last update: 2018-05-20  (by Dikra)

using UnityEngine;
using UnityEngine.UI;

using SimpleFirebaseUnity;
using SimpleFirebaseUnity.MiniJSON;

using System.Collections.Generic;
using System.Collections;
using System;


public class LoginScript : MonoBehaviour
{
    object mSnapshot = null;
    bool usernameWasInvalid = false;
    static int debug_idx = 0;

    //MOVE ELSEWHERE
    string myName = "";

    Firebase fbRoot;

    public Text mErrorText;

    public Text mNameField, mPasswordField;

    public GameObject mScreenCover;

    string tempPasswordFromDatabase = "";
    string tempPasswordFromUser = "";

    // Use this for initialization
    void Start()
    {
        StartCoroutine(StartFirebase());
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            UpdateCurrencyInDB(69);
        }
    }

    void UpdateCurrencyInDB(int newMoney)
    {
        Dictionary<string, object> tDict = new Dictionary<string, object>();
        tDict.Add("Money F2P", newMoney);

        FirebaseQueue fq = new FirebaseQueue(true, 3, 1f);
        fq.AddQueueUpdate(fbRoot.Child("Accounts/" + myName, true), tDict);
    }

    IEnumerator StartFirebase()
    {
        fbRoot = Firebase.CreateNew("https://TooTiredTest.firebaseio.com", "zTb0rNHGYxM7lEa1DWojJW9sObs5fFYkaJoiSUaQ");

        fbRoot.OnGetSuccess += GetOKHandler;
        fbRoot.OnGetFailed += GetFailHandler;
        fbRoot.OnSetSuccess += SetOKHandler;
        fbRoot.OnSetFailed += SetFailHandler;
        fbRoot.OnUpdateSuccess += UpdateOKHandler;
        fbRoot.OnUpdateFailed += UpdateFailHandler;
        fbRoot.OnPushSuccess += PushOKHandler;
        fbRoot.OnPushFailed += PushFailHandler;
        fbRoot.OnDeleteSuccess += DelOKHandler;
        fbRoot.OnDeleteFailed += DelFailHandler;

        fbRoot.GetValue();

        yield return null;
    }

    //Button calls this
    public void ButtonAttemptLogin()
    {
        //ensure username and password fields contain (valid) content
        myName = mNameField.text;
        tempPasswordFromUser = mPasswordField.text;
        tempPasswordFromDatabase = "";

        if (myName.Length > 0 && tempPasswordFromUser.Length > 0)
        {
            mScreenCover.SetActive(true);
            AttemptLogin(myName);
        }
        else
        {
            mErrorText.text = "Please enter a Username and Password.";
        }
    }

    //Button calls this
    public void ButtonAttemptSignup()
    {
        //ensure username and password fields contain (valid) content
        myName = mNameField.text;
        tempPasswordFromUser = mPasswordField.text;
        tempPasswordFromDatabase = "";

        if (myName.Contains(" "))
        {
            LoginFailed("Invalid Username. Cannot contain Spaces");
            return;
        }

        string lowerNameCheck = myName.ToLower();
        if (lowerNameCheck.Length > 3)
        {
            lowerNameCheck = lowerNameCheck.Remove(3, lowerNameCheck.Length - 3);
        }

        if (lowerNameCheck.Contains("tts"))
        {
            LoginFailed("Invalid Username. Cannot start with TTS");
            return;
        }

        if (myName.Length > 0 && tempPasswordFromUser.Length > 0)
        {
            mScreenCover.SetActive(true);
            AttemptSignup(myName);
        }
        else
        {
            mErrorText.text = "Please enter a Username and Password.";
        }
    }

    void FoundUsernameForSignup(Firebase sender, DataSnapshot snapshot)
    {
        if (snapshot.RawValue == null)
        {
            SignupWithCurrentInfo();
        }
        else
        {
            LoginFailed("Username already exists.");
        }
    }

    void NoExistingData(Firebase sender, FirebaseError error)
    {
        SignupWithCurrentInfo();  
    }

    void SignupWithCurrentInfo()
    {
        Dictionary<string, object> newUser = GenerateNewUser();
        newUser["Password"] = tempPasswordFromUser;

        FirebaseQueue fq = new FirebaseQueue(true, 3, 1f);
        fq.AddQueueUpdate(fbRoot.Child("Accounts/" + myName, true), newUser);

        LoginFailed("Signed up! Logging in...");

        mScreenCover.SetActive(true);

        StartCoroutine(LoginDelay());
    }

    IEnumerator LoginDelay(float time = 1.5f)
    {
        yield return new WaitForSeconds(time);
        ButtonAttemptLogin();
    }

    Dictionary<string, object> GenerateNewUser()
    {
        Dictionary<string, object> newUser = new Dictionary<string, object>();

        //Dictionary<string, object> f2pMoney = new Dictionary<string, object>();
        //Dictionary<string, object> p2pMoney = new Dictionary<string, object>();
        //Dictionary<string, object> password = new Dictionary<string, object>();
        //Dictionary<string, object> adminStat = new Dictionary<string, object>();

        newUser.Add("Money F2P", 50);
        newUser.Add("Money P2P", 10);
        newUser.Add("Admin", false);
        newUser.Add("Password", "password");

        return newUser;
    }

    void FoundUsername(Firebase sender, DataSnapshot snapshot)
    {
        if (snapshot.RawValue == null)
        {
            LoginFailed("Invalid Username");
        }
        else
        {
            //store info (just in case)
            mSnapshot = snapshot.RawValue;


            Dictionary<string, object> tempDict = (Dictionary<string, object>)mSnapshot;
            if (tempDict["Password"].ToString() == tempPasswordFromUser)
            {
                //login worked, wow
                //Uncover the screen
                mScreenCover.SetActive(false);

                //Get GameManager and input rest of tempDict info.
                int fMoney, pMoney;
                fMoney = Convert.ToInt16(tempDict["Money F2P"]);
                pMoney = Convert.ToInt16(tempDict["Money P2P"]);
                LoadGameManagerWithData(myName, fMoney, (bool)tempDict["Admin"], pMoney);

                mErrorText.text = "Success!";

                mPasswordField.text = "";

                //Change GameObjects on the screen, maybe calling a different object's function
                FindObjectOfType<MainMenuControllerScript>().UpdatePlayerUI(myName, fMoney, pMoney);
                FindObjectOfType<MainMenuControllerScript>().LoggedIn();
            }
            else
            {
                LoginFailed("Username and Password do not match");
            }
        }
    }

    void LoadGameManagerWithData(string cName, int f2pM, bool isAdmin = false, int cPremiumMoney = 0)
    {
        if (FindObjectOfType<GMS>())
        {
            GMS tempGMS = FindObjectOfType<GMS>();
            tempGMS.username = cName;
            tempGMS.currency = f2pM;
            tempGMS.premiumCurrency = cPremiumMoney;
            tempGMS.adminRights = isAdmin;
        }
    }

    void FailedToFindUsername(Firebase sender, FirebaseError err)
    {
        LoginFailed("Invalid Username");
        Debug.Log("Could not access username or database");
    }

    void GetOKHandler(Firebase sender, DataSnapshot snapshot)
    {
        DebugLog("[OK] Get from key: <" + sender.FullKey + ">");
        DebugLog("[OK] Raw Json: " + snapshot.RawJson);

        Dictionary<string, object> dict = snapshot.Value<Dictionary<string, object>>();
        List<string> keys = snapshot.Keys;

        if (keys != null)
            foreach (string key in keys)
            {
                DebugLog(key + " = " + dict[key].ToString());
            }
    }

    void GetFailHandler(Firebase sender, FirebaseError err)
    {
        usernameWasInvalid = true;
        Debug.Log("Could not access username or database");
        //DebugError("[ERR] Get from key: <" + sender.FullKey + ">,  " + err.Message + " (" + (int)err.Status + ")");
    }

    void SetOKHandler(Firebase sender, DataSnapshot snapshot)
    {
        DebugLog("[OK] Set from key: <" + sender.FullKey + ">");
    }

    void SetFailHandler(Firebase sender, FirebaseError err)
    {
        DebugError("[ERR] Set from key: <" + sender.FullKey + ">, " + err.Message + " (" + (int)err.Status + ")");
    }

    void UpdateOKHandler(Firebase sender, DataSnapshot snapshot)
    {
        DebugLog("[OK] Update from key: <" + sender.FullKey + ">");
    }

    void UpdateFailHandler(Firebase sender, FirebaseError err)
    {
        DebugError("[ERR] Update from key: <" + sender.FullKey + ">, " + err.Message + " (" + (int)err.Status + ")");
    }

    void DelOKHandler(Firebase sender, DataSnapshot snapshot)
    {
        DebugLog("[OK] Del from key: <" + sender.FullKey + ">");
    }

    void DelFailHandler(Firebase sender, FirebaseError err)
    {
        DebugError("[ERR] Del from key: <" + sender.FullKey + ">, " + err.Message + " (" + (int)err.Status + ")");
    }

    void PushOKHandler(Firebase sender, DataSnapshot snapshot)
    {
        DebugLog("[OK] Push from key: <" + sender.FullKey + ">");
    }

    void PushFailHandler(Firebase sender, FirebaseError err)
    {
        DebugError("[ERR] Push from key: <" + sender.FullKey + ">, " + err.Message + " (" + (int)err.Status + ")");
    }

    void GetRulesOKHandler(Firebase sender, DataSnapshot snapshot)
    {
        DebugLog("[OK] GetRules");
        DebugLog("[OK] Raw Json: " + snapshot.RawJson);
    }

    void GetRulesFailHandler(Firebase sender, FirebaseError err)
    {
        DebugError("[ERR] GetRules,  " + err.Message + " (" + (int)err.Status + ")");
    }

    void GetTimeStamp(Firebase sender, DataSnapshot snapshot)
    {
        long timeStamp = snapshot.Value<long>();
        DateTime dateTime = Firebase.TimeStampToDateTime(timeStamp);

        DebugLog("[OK] Get on timestamp key: <" + sender.FullKey + ">");
        DebugLog("Date: " + timeStamp + " --> " + dateTime.ToString());
    }

    void DebugLog(string str)
    {
        Debug.Log(str);
        //if (textMesh != null)
        //{
        //    textMesh.text += (++debug_idx + ". " + str) + "\n";
        //}
    }

    void DebugWarning(string str)
    {
        Debug.LogWarning(str);
        //if (textMesh != null)
        //{
        //    textMesh.text += (++debug_idx + ". " + str) + "\n";
        //}
    }

    void DebugError(string str)
    {
        Debug.LogError(str);
        //if (textMesh != null)
        //{
        //    textMesh.text += (++debug_idx + ". " + str) + "\n";
        //}
    }

    IEnumerator GetDataFromUsername(string cUser)
    {
        // Get child node from firebase, if false then all the callbacks are not inherited.
        Firebase mAccount = fbRoot.Child("Accounts/" + cUser);

        mAccount.OnGetSuccess += GetUserData;
        mAccount.OnGetFailed += GetFailHandler;

        // Unnecessarily skips a frame, really, unnecessary.
        yield return null;

        mAccount.GetValue();
        yield break;
    }

    void GetUserData(Firebase sender, DataSnapshot snapshot)
    {
        //Debug.Log("test raw json: " + snapshot.RawJson);       
        //Debug.Log("test raw value: " + snapshot.RawValue);

        mSnapshot = snapshot.RawValue;
        Debug.Log("we hit this");
       
        if (mSnapshot == null)
        {
            usernameWasInvalid = true;
        }

        //Dictionary<string, object> allParams = (Dictionary<string,object>)(snapshot.RawValue);

        //adminStatus = (bool)allParams["Admin"];        
        //myMoney = Convert.ToInt16(allParams["Money F2P"]); //THIS WORKS@!!!!

        //Debug.Log(myName + " admin status: " + adminStatus);
        //Debug.Log(myName + " f2p cash: " + myMoney);
    }

    void LoginFailed(string cError)
    {
        //Uncover the screen
        mScreenCover.SetActive(false);
        //Update Error Message: Cannot find username
        mErrorText.text = cError;

        usernameWasInvalid = false; //reset this variable
    }

    void AttemptSignup(string cUser)
    {
        Firebase mAccount = fbRoot.Child("Accounts/" + cUser);

        mAccount.OnGetSuccess += FoundUsernameForSignup;
        mAccount.OnGetFailed += NoExistingData;

        mAccount.GetValue();
        Debug.Log("Polled Database for Account Info (to sign up)");
    }

    void AttemptLogin(string cUser)
    {
        // Get child node from firebase, if false then all the callbacks are not inherited.
        Firebase mAccount = fbRoot.Child("Accounts/" + cUser);

        mAccount.OnGetSuccess += FoundUsername;
        mAccount.OnGetFailed += FailedToFindUsername;

        mAccount.GetValue();
        Debug.Log("Polled Database for Account Info");
    }

    IEnumerator AttemptLogiadawdn(string cName, string cPass)
    {
        //Cover the screen
        //mScreenCover.SetActive(true);

        //StartCoroutine(GetDataFromUsername(cName));

        //Do not progress until we've hit the database up!
        while (mSnapshot == null && !usernameWasInvalid)
        {
            //yield return new WaitForEndOfFrame();
            yield return null;
        }

        if (usernameWasInvalid)
        {
            //Uncover the screen
            mScreenCover.SetActive(false);
            //Update Error Message: Cannot find username
            mErrorText.text = "Username was invalid";
            usernameWasInvalid = false; //reset this variable
            yield break;
        }

        //Parse through the Snapshot, ensuring the passwords match
        Dictionary<string, object> tempDict = (Dictionary<string, object>)mSnapshot;
        if (tempDict["Password"].ToString() != cPass)
        {
            //If not, error that out
            //Uncover the screen
            mScreenCover.SetActive(false);
            //Update Error Message: Username and Password do not match
            mErrorText.text = "Username and Password do not match";
        }
        else //LOG ME IN!!! hamachi.
        {
            //If so, login, updating information on the screen and in the GameManager
            //Uncover the screen
            mScreenCover.SetActive(false);

            //Get GameManager and input rest of tempDict info.

            mErrorText.text = "Success!";
        }

        yield break;
        

    }

    public void PostCurrencyWithUsername(FirebaseQueue fq, Firebase firebase, string cName, int cMoney)
    {
        Dictionary<string, object> tempScoreUpdate = new Dictionary<string, object>();
        Dictionary<string, object> currencyHolder = new Dictionary<string, object>();

        currencyHolder.Add("Money F2P", cMoney);
        tempScoreUpdate.Add(cName, currencyHolder);

        fq.AddQueueUpdate(firebase.Child("Accounts", true), tempScoreUpdate);
        //firebaseQueue.AddQueueSetTimeStamp(firebase, "lastUpdate"); //idc about this rn
    }



    Dictionary<string, object> GetSampleScoreBoard()
    {
        Dictionary<string, object> scoreBoard = new Dictionary<string, object>();
        Dictionary<string, object> scores = new Dictionary<string, object>();
        Dictionary<string, object> p1 = new Dictionary<string, object>();
        Dictionary<string, object> p2 = new Dictionary<string, object>();
        Dictionary<string, object> p3 = new Dictionary<string, object>();

        p1.Add("name", "simple");
        p1.Add("score", 80);

        p2.Add("name", "firebase");
        p2.Add("score", 100);

        p3.Add("name", "csharp");
        p3.Add("score", 60);

        scores.Add("p1", p1);
        scores.Add("p2", p2);
        scores.Add("p3", p3);

        scoreBoard.Add("scores", scores);

        scoreBoard.Add("layout", Json.Deserialize("{\"x\": 0, \"y\":10}") as Dictionary<string, object>);
        scoreBoard.Add("resizable", true);

        scoreBoard.Add("temporary", "will be deleted later");

        return scoreBoard;
    }
}