﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;
using UnityEngine.UI;

public class ChoiceChecker : UnityEngine.MonoBehaviour {

    float roomLoadBuffer = 1.0f;
    bool roomHasLoaded = false;

    public Text myChoice, theirChoice;
    public Text myName, theirName;
    public Text winText, winCountText;

    NetworkedRoomLoader localNR;
    NetworkedRoomLoader opponentNR;

    public enum GameChoices
    {
        NONE,
        ROCK,
        PAPER,
        SCISSORS,
    }

    public GameChoices mChoice = GameChoices.NONE;
    GameChoices opponentChoice = GameChoices.NONE;

    int totalWins = 0;

    public enum RoundState
    {
        WAITING,
        DISPLAYING,
        END,
    }

    RoundState mRoundState = RoundState.WAITING;

    float timeBetweenRounds = 1.0f;

    // Use this for initialization
    void Start ()
    {
        opponentChoice = GameChoices.NONE;
    }

    // Update is called once per frame
    void Update ()
    {
        roomLoadBuffer -= Time.deltaTime;
        if (roomLoadBuffer <= 0 && !roomHasLoaded)
        {
            PhotonNetwork.Instantiate("NetworkedObject", Vector3.zero, Quaternion.identity, 0);
            roomHasLoaded = true;
        }

        //myChoice.text = mChoice.ToString();

        if (mRoundState == RoundState.WAITING)
        {
            if (mChoice == GameChoices.NONE && opponentChoice != GameChoices.NONE)
            {
                //the opponent is waiting for YOU
                myChoice.text = "Choose!";
                theirChoice.text = "Hidden";
            }
            else if ((localNR != null && opponentNR != null) && localNR.hasReceivedEnemyScore && opponentNR.hasReceivedEnemyScore)
            {
                mRoundState = RoundState.DISPLAYING;
                theirChoice.text = opponentChoice.ToString();
                myChoice.text = mChoice.ToString();
            }
            //else if (mChoice != GameChoices.NONE && opponentChoice != GameChoices.NONE)
            //{
            //    mRoundState = RoundState.DISPLAYING;
            //    myChoice.text = mChoice.ToString();
            //    theirChoice.text = opponentChoice.ToString();
            //}
        }

        if (mRoundState == RoundState.DISPLAYING)
        {
            int tempChoice = (int)mChoice;
            int tempChoice2 = (int)opponentChoice;

            //theirChoice.text = opponentChoice.ToString();
            //theirChoice.text = opponentChoice.ToString();

            tempChoice -= 1;
            tempChoice2 -= 1;

            if (tempChoice == tempChoice2)
            {
                winText.text = "TIE!";
            }
            else if (tempChoice == ((tempChoice2 + 1) % 3))
            {
                winText.text = "WIN!";
                totalWins++;
                winCountText.text = totalWins.ToString();
            }
            else
            {
                winText.text = "LOSE :(";
            }

            mRoundState = RoundState.END;
            timeBetweenRounds = 1.0f;


            mChoice = GameChoices.NONE; //this will be sent over the next second to the opponent so they don't start the next round thinking you played a move already
            if (localNR != null)
            {
                localNR.mChoice = mChoice;                
            }
        }

        if (mRoundState == RoundState.END)
        {
            timeBetweenRounds -= Time.deltaTime;
            if (timeBetweenRounds <= 0)
            {
                mRoundState = RoundState.WAITING;
                mChoice = GameChoices.NONE;
                opponentChoice = GameChoices.NONE;
                myChoice.text = "NONE";
                theirChoice.text = "NONE";
                localNR.hasReceivedEnemyScore = false;
            }
        }

        if (roomHasLoaded)
        {
            if (mRoundState != RoundState.END)
            {
                foreach (NetworkedRoomLoader nr in FindObjectsOfType<NetworkedRoomLoader>())
                {
                    if (nr.GetComponent<PhotonView>().owner != null && nr.GetComponent<PhotonView>().owner.IsLocal)
                    {
                        nr.mChoice = mChoice;
                        localNR = nr;
                        //myName.text = nr.playerName + "\'s Choice";
                    }
                    else
                    {
                        if (localNR != null && !localNR.hasReceivedEnemyScore)
                        {
                            if (opponentChoice == GameChoices.NONE)
                            {
                                opponentChoice = nr.mChoice;
                                if (opponentChoice != GameChoices.NONE)
                                {
                                    localNR.hasReceivedEnemyScore = true;
                                }
                            }

                            opponentNR = nr;                            
                        }

                        //if (mRoundState == RoundState.DISPLAYING)
                        //    theirChoice.text = nr.mChoice.ToString();

                        //theirName.text = nr.playerName + "\'s Choice";                       
                    }
                }
            }
            else //wait for animations and stuff to finish, verify we should continue, then reset.
            {
                opponentChoice = GameChoices.NONE;
                mChoice = GameChoices.NONE;

                //if (localNR != null)
                //{
                //    localNR.mChoice = GameChoices.NONE;
                //}
            }
        }
        else
        {
            theirChoice.text = "Waiting";            
        }
    }

    public void ChooseOption(int cID)
    {
        if (mRoundState == RoundState.WAITING && mChoice == GameChoices.NONE)
        {
            mChoice = (ChoiceChecker.GameChoices)(cID + 1);

            if (localNR != null)
            {
                localNR.mChoice = mChoice;
            }

            myChoice.text = mChoice.ToString();
        }
    }
}
