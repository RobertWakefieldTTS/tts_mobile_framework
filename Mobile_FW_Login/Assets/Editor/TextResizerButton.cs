﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class TextResizerButton {


    //[MenuItem("Tools/UI/Enhance Text")]
    //private static void NewMenuOption()
    //{
    //    PlayerPrefs.DeleteAll();
    //}
}

public class TextResizerWindow : EditorWindow
{
    float enhanceMult = 1.0f;

    // Add menu named "My Window" to the Window menu
    [MenuItem("Tools/UI/Enhance Text (Window)")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        TextResizerWindow window = (TextResizerWindow)EditorWindow.GetWindow(typeof(TextResizerWindow));
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Enhance Text to make it clearer.", EditorStyles.boldLabel);
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("Very high quality text *could* cause FPS reduction.", EditorStyles.boldLabel);
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("Multiply by...", EditorStyles.boldLabel);
        enhanceMult = EditorGUILayout.FloatField(enhanceMult);
        GUILayout.EndHorizontal();

        if (GUILayout.Button("ENHANCE!") && enhanceMult != 0)
        {
            if (Selection.gameObjects.Length >= 1)
            {
                float tX, tY, tZ, tW, tH, tFs; //the scale values, widthheight, and font size
                Text tempText;
                RectTransform tempRect;

                foreach (GameObject g in Selection.gameObjects)
                {
                    if (g.GetComponent<Text>() && g.GetComponent<RectTransform>())
                    {
                        //grab values
                        tempText = g.GetComponent<Text>();
                        tempRect = g.GetComponent<RectTransform>();
                        tX = tempRect.localScale.x;
                        tY = tempRect.localScale.y;
                        tZ = tempRect.localScale.z;
                        tW = tempRect.rect.width;
                        tH = tempRect.rect.height;
                        tFs = tempText.fontSize;

                        //multiply
                        tFs *= enhanceMult;
                        tX *= 1.0f / enhanceMult;
                        tY *= 1.0f / enhanceMult;
                        tZ *= 1.0f / enhanceMult;
                        tW *= enhanceMult;
                        tH *= enhanceMult;

                        //note that text may hit 300 and debug if it does
                        if (tFs >= 300)
                        {
                            tFs = 300;
                            Debug.Log("Maximum font size of 300 hit!");
                        }

                        //apply values
                        tempText.fontSize = (int)tFs;
                        tempRect.sizeDelta = new Vector2(tW, tH);
                        tempRect.localScale = new Vector3(tX, tY, tZ);

                    }
                    else
                    {
                        Debug.Log("GameObject selected missing Text/RectTransform component(s)");
                    }
                }
            }
            else
            {
                Debug.Log("You must select a Text object in the scene!");
            }
        }
        else if (enhanceMult == 0)
        {
            Debug.Log("This tool will not Zero out an object!");
        }
    }
}
