﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TextToStars : MonoBehaviour {

    public Text targetTextToMakeStars;
    Text myText;

	// Use this for initialization
	void Start ()
    {
        myText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        targetTextToMakeStars.text = "";

        for (int i = 0; i < myText.text.Length; ++i)
        {
            targetTextToMakeStars.text += "*";
        }
	}
}
