﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuControllerScript : MonoBehaviour {

    public Text nameText, fMoneyText, pMoneyText;

    public GameObject loginParent, mainParent;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LoggedOut()
    {
        mainParent.SetActive(false);
        loginParent.SetActive(true);
    }

    public void LoggedIn()
    {
        mainParent.SetActive(true);
        loginParent.SetActive(false);
    }

    public void UpdatePlayerUI(string cName, int fMoney, int pMoney)
    {
        nameText.text = cName;
        fMoneyText.text = fMoney.ToString();
        pMoneyText.text = pMoney.ToString();
    }
}
